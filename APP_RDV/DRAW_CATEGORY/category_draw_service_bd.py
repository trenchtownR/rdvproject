# OM 2698.03.21 Permet d'insérer plusieurs valeurs dans la table t_films
from datetime import datetime

import pymysql

from APP_RDV.DATABASE import connect_db_context_manager


class Category_Draw_Service():
    def __init__(self):
        print("Classe constructeur Draw_Service ")

    def afficher_draw_with_categoryes (self, id_selected_draw):
        print("id_film_selected  ", id_selected_draw)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_genres"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_concat_category_draw = """SELECT id_draw, draw_name, draw_description, image,
                                                            GROUP_CONCAT(category_name) as CategoryOfDraw FROM t_draw_to_category AS T1
                                                            RIGHT JOIN t_draw AS T2 ON T2.id_draw = T1.fk_draw
                                                            LEFT JOIN t_category AS T3 ON T3.id_category = T1.fk_category
                                                            GROUP BY id_draw"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".

            # with connect_db_context_manager.connexion_bd.cursor() as mc_afficher:
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as mc_afficher:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    if id_selected_draw == 0:
                        mc_afficher.execute(strsql_concat_category_draw)
                    else:
                        # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                        valeur_id_draw_dictionnaire = {"value_selected_id_draw": id_selected_draw}
                        strsql_concat_category_draw += """ HAVING id_draw= %(value_selected_id_draw)s"""
                        # Envoi de la commande MySql
                        mc_afficher.execute(strsql_concat_category_draw, valeur_id_draw_dictionnaire)

                # Récupère les données de la requête.
                data_category_of_draw_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_genres_films_afficher_concat ", data_category_of_draw_concat, " Type : ",
                      type(data_category_of_draw_concat))

                # Retourne les données du "SELECT"
                return data_category_of_draw_concat

        except Exception as erreur:
            print("error message: {0}".format(erreur))

    # TODO
        # except pymysql.Error as erreur:
        #     print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
        #     # raise MaBdErreurPyMySl(
        #     #     f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        # except Exception as erreur:
        #     print(f"DGGF gfadc Exception {erreur.args}")
        #     # raise MaBdErreurConnexion(
        #     #     f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        # except pymysql.err.IntegrityError as erreur:
        #     print(f"DGGF integrity Exception {erreur.args}")
        #     # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
        #     # Ainsi on peut avoir un message d'erreur personnalisé.
        #     # raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")
    def get_binded_categoryes(self, id_selected_draw):

        strsql_slected_draw = "SELECT * FROM `t_draw` WHERE id_draw = %(id_draw_selected)s"

        strsql_slect_binded_category = """SELECT id_draw, id_category, category_name FROM t_draw_to_category AS T1
                                            INNER JOIN t_draw AS T2 ON T2.id_draw = T1.fk_draw
                                            INNER JOIN t_category AS T3 ON T3.id_category = T1.fk_category
                                            WHERE id_draw = %(id_draw_selected)s"""

        strsql_slect_not_binded_category = """SELECT id_category, category_name FROM t_category
                                                    WHERE id_category not in(SELECT id_category as idCategoryToFilm FROM t_draw_to_category AS T1
                                                    INNER JOIN t_draw AS T2 ON T2.id_draw = T1.fk_draw
                                                    INNER JOIN t_category AS T3 ON T3.id_category = T1.fk_category
                                                    WHERE id_draw = %(id_draw_selected)s)"""

        with connect_db_context_manager.MaBaseDeDonne() as conn1:
            with conn1.cursor() as mc_afficher:

                mc_afficher.execute(strsql_slected_draw, id_selected_draw)
                data_draw = mc_afficher.fetchall()

                mc_afficher.execute(strsql_slect_binded_category, id_selected_draw)
                data_binded_categorys_to_draw = mc_afficher.fetchall()

                mc_afficher.execute(strsql_slect_not_binded_category, id_selected_draw)
                data_not_binded_categorys_to_draw = mc_afficher.fetchall()


        return data_draw, data_binded_categorys_to_draw, data_not_binded_categorys_to_draw

    def add_category_to_draw(self, values_category_draw_dict):

        try:
            print(values_category_draw_dict)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_insert_genre_film = """INSERT INTO t_draw_to_category (id_draw_category, fk_draw, fk_category)
                                                        VALUES (NULL, %(value_fk_draw)s, %(value_fk_category)s)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as mc_afficher:
                    mc_afficher.execute(strsql_insert_genre_film, values_category_draw_dict)

        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            print(erreur)
        pass

    def delete_category_of_draw(self, values_category_draw_dict):

        try:
            print(values_category_draw_dict)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_category_ofDraw = """DELETE FROM t_draw_to_category WHERE fk_draw = %(value_fk_draw)s AND fk_category = %(value_fk_category)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as mc_afficher:
                    mc_afficher.execute(strsql_delete_category_ofDraw, values_category_draw_dict)

        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            print(erreur)

        pass
