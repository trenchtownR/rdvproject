# controller_category.py
# OM 2020.04.06 Gestions des "USER" FLASK pour les genres.
# copié et readapté par Luca Maggioli info 1b

import pymysql
from flask import render_template, flash, request, session, url_for
from werkzeug.utils import redirect

from APP_RDV import obj_mon_application

from APP_RDV.DRAW_CATEGORY.category_draw_service_bd import Category_Draw_Service

# -------------------------------
@obj_mon_application.route("/afficher_draw_with_category/<int:id_draw_selected>", methods=['GET', 'POST'])
def afficher_draw_with_category(id_draw_selected):

    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "GET":
        try:

            obj_draw_service = Category_Draw_Service()

            # id_draw_selected = request.form['id_draw']

            # data_draw = obj_draw_service.afficher_draw()
            data_draw_with_categoryes = obj_draw_service.afficher_draw_with_categoryes(id_draw_selected)

            # print(" data gentre s", data_draw, "type ", type(data_draw))
            print(" data gentre s", data_draw_with_categoryes, "type ", type(data_draw_with_categoryes))

            # # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            message_html = 'Données Listées !!'
            print(' Données Listées !!')
        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as error:
            # OM 2019.03.25 Les 2 messages ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", error)
            flash("Gros problème dans l'affichage des genres  !", "Danger")


    return render_template("draw_category/afficher_draw_with_category.html", data=data_draw_with_categoryes)

@obj_mon_application.route("/edit_category_of_draw", methods=['GET', 'POST'])
def edit_category_of_draw():
    try:
        obj_draw_cat_service = Category_Draw_Service()

        id_draw_selected = request.values['id_draw_selected_html']
        session['session_id_draw_selected'] = id_draw_selected

        dict_id_draw = {"id_draw_selected": id_draw_selected}



        data_selected_draw, category_assigned_to_draw, category_not_assigned = obj_draw_cat_service.get_binded_categoryes(dict_id_draw)

        old_category_not_assigned = [item['id_category'] for item in category_not_assigned]
        session['session_old_category_not_assigned'] = old_category_not_assigned

        old_category_assigned_to_draw = [item['id_category'] for item in category_assigned_to_draw]
        session['session_old_category_assigned_to_draw'] = old_category_assigned_to_draw

    except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
            TypeError) as error:
        # OM 2019.03.25 Les 2 messages ci-après permettent de donner un sentiment rassurant aux utilisateurs.
        # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
        print("Problème avec la BD ! : %s", error)
        flash("Gros problème dans l'affichage des genres  !", "Danger")

    return render_template("draw_category/edit_category_of_draw.html",
                           data_actual_draw=data_selected_draw,
                           data_category_assigned_to_draw=category_assigned_to_draw,
                           data_category_not_assigned_to_draw=category_not_assigned)


@obj_mon_application.route("/update_category_of_draw", methods=['GET', 'POST'])
def update_category_of_draw():
    id_draw_selected = session['session_id_draw_selected']

    old_category_not_assigned = session['session_old_category_not_assigned']

    old_category_assigned_to_draw = session['session_old_category_assigned_to_draw']

    new_lst_str_category = request.form.getlist('name_select_tags')

    new_lst_int_id_category = list(map(int, new_lst_str_category))

    session.clear()

    list_deleted_categoryes = list(
        set(old_category_assigned_to_draw) - set(new_lst_int_id_category))

    lst_category_added = list(
        set(new_lst_int_id_category) - set(old_category_assigned_to_draw))

    obj_draw_cat_service = Category_Draw_Service()

    for id_added_categoryes in lst_category_added:
        # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
        # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
        values_category_draw_dict = {"value_fk_draw": id_draw_selected,
                                     "value_fk_category": id_added_categoryes}
        # Insérer une association entre un(des) genre(s) et le film sélectionner.
        obj_draw_cat_service.add_category_to_draw(values_category_draw_dict)


    for id_deleted_categoryes in list_deleted_categoryes:
        # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
        # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
        values_category_draw_dict = {"value_fk_draw": id_draw_selected,
                                     "value_fk_category": id_deleted_categoryes}
        # Insérer une association entre un(des) genre(s) et le film sélectionner.
        obj_draw_cat_service.delete_category_of_draw(values_category_draw_dict)

    data_draw_with_categoryes = obj_draw_cat_service.afficher_draw_with_categoryes(id_draw_selected)

    return render_template("draw_category/afficher_draw_with_category.html", data=data_draw_with_categoryes)