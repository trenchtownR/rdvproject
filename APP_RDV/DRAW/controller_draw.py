# controller_category.py
# OM 2020.04.06 Gestions des "USER" FLASK pour les genres.
# copié et readapté par Luca Maggioli info 1b

import pymysql
from flask import render_template, flash, request, jsonify

from APP_RDV import obj_mon_application
from APP_RDV.DRAW.draw_service_bd import Draw_Service

# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /add_genres ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# Pour la tester http://127.0.0.1:1234/add_genres
# ---------------------------------------------------------------------------------------------------

@obj_mon_application.route("/add_draw", methods=['GET', 'POST'])
def add_draw():

    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:

            obj_draw_service = Draw_Service()
            # OM 2019.03.17 Récupère le contenu du champ dans le formulaire HTML "add_category.html"
            draw_name = request.form['name_draw_html']
            draw_description = request.form['draw_description_html']
            draw_image = request.form['image_draw_html']

            draw_image ="/static/images/draws/" + draw_image

            valeurs_insertion_dictionnaire = {"draw_name": draw_name, "draw_description": draw_description, "draw_image": draw_image}

            #valeurs_insertion_dictionnaire = {"value_intitule_genre": draw_name}

            # obj_draw_service.add_draw(draw_name, draw_image, draw_description)
            obj_draw_service.add_draw(valeurs_insertion_dictionnaire)

            # # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash("Données insérées!! Inserer le fichier de l'image dans le dossier ""/static/images/draw/""", "Sucess")

            print(' Données insérées !!')

        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as error:
            # OM 2019.03.25 Les 2 messages ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", error)
            flash("Gros problème dans l'ajout de user", "Danger")
    #elif request.method == "GET":
        #obj_draw_service = gestionUser()
        #data_roles = obj_draw_service.get_roles()
        #print(" data gentre s", data_roles, "type ", type(data_roles))

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    #return render_template("draw/add_category.html", data=data_draw)
    return render_template("draw/add_draw.html")

# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /afficher_genres ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# Pour la tester http://127.0.0.1:1234/afficher_genres
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/afficher_draw", methods=['GET', 'POST'])
def afficher_draw():

    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "GET":
        try:

            obj_draw_service = Draw_Service()

            data_draw = obj_draw_service.afficher_draw()
            print(" data gentre s", data_draw, "type ", type(data_draw))

            # # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            message_html = 'Données Listées !!'
            print(' Données Listées !!')
        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as error:
            # OM 2019.03.25 Les 2 messages ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", error)
            flash("Gros problème dans l'affichage des genres  !", "Danger")


    # OM 2020.04.07 Envoie la page "HTML" au serveur. On passe des paramètres nécessaires à l'affichages des données
    # contenues dans la BD.
    return render_template("draw/afficher_draw.html", data=data_draw)
