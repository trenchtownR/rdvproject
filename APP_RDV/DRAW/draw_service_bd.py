# OM 2698.03.21 Permet d'insérer plusieurs valeurs dans la table t_films
from datetime import datetime

import pymysql

from APP_RDV.DATABASE import connect_db_context_manager


class Draw_Service():
    def __init__(self):
        print("Classe constructeur Draw_Service ")


    def afficher_draw(self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            strsql_affiche_draws = "SELECT * FROM t_draw"
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(strsql_affiche_draws)
                    data_draw = cursor.fetchall()
                    print("data_draw : ", data_draw)
                    return data_draw
        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))

    # def add_draw(self, _draw_name, _draw_image, draw_description):
    def add_draw(self, valeurs_insertion_dictionnaire):
        try:
            # print("Data from html: '" + _draw_name + "' & '" + _draw_image + "'" + draw_description + "'")
            print(valeurs_insertion_dictionnaire)

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            #strsql_insert_genre = "INSERT INTO t_user (Name, Surname, BirthDate) VALUES ('"+_user_Name+"', '"+_user_Surname+"', "+bday_str+")"
            # strsql_insert_genre = "INSERT INTO t_draw (draw_name, image, draw_description) VALUES ('"+_draw_name+"', '"+_draw_image+"', '"+draw_description+"')"
            strsql_insert_genre = "INSERT INTO t_draw (draw_name, draw_description, image) VALUES (%(draw_name)s, %(draw_description)s, %(draw_image)s)"
            with connect_db_context_manager.MaBaseDeDonne() as db:
                db.cursor().execute(strsql_insert_genre, valeurs_insertion_dictionnaire)

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))


