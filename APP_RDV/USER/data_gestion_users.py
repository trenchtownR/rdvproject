# OM 2698.03.21 Permet d'insérer plusieurs valeurs dans la table t_films
from datetime import datetime

from APP_RDV.DATABASE import connect_db_context_manager


class gestionUser():
    def __init__(self):
        print("Classe constructeur GestionGenres ")

    def get_roles(self):
        try:
            strsql_genres_afficher = "SELECT role_name FROM t_role"
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(strsql_genres_afficher)
                    roles = cursor.fetchall()
                    print("data_users : ", roles)
                    return roles

        except Exception as erreur:
            print("something went wrong: "+ format(erreur))

    def afficher_users(self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            strsql_genres_afficher = "SELECT * FROM t_user"
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(strsql_genres_afficher)
                    data_users = cursor.fetchall()
                    print("data_users : ", data_users)
                    return data_users
        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))

    def add_user(self, _user_Name, _user_Surname, bday_str): # _user_BirthDate
        try:
            print("Data from html: '" + _user_Name + "' & '" + _user_Surname+"' & '"+ bday_str +"'")
            date_time_bday = datetime.strptime(bday_str, '%Y-%m-%d')

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            #strsql_insert_genre = "INSERT INTO t_user (Name, Surname, BirthDate) VALUES ('"+_user_Name+"', '"+_user_Surname+"', "+bday_str+")"
            strsql_insert_genre = "INSERT INTO t_user (Name, Surname, BirthDate) VALUES ('"+_user_Name+"', '"+_user_Surname+"', %s)"
            with connect_db_context_manager.MaBaseDeDonne() as db:
                db.cursor().execute(strsql_insert_genre, date_time_bday.strftime('%Y-%m-%d'))

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))

    def add_user_role(self, _role_name):
        try:
            print("Data from html: '" + _role_name + "'")

            strsql_select_genreID = "SELECT id_role FROM t_role WHERE role_name = '" + _role_name + "'"
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(strsql_select_genreID)
                    role_id = cursor.fetchall()
                    print("data_users : ", role_id)


            # strsql_insert_genre = "INSERT INTO t_user_to_role (fk_role, fk_user) VALUES ('" + role_id + "', '" + _user_Surname + "', %s)"

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))
