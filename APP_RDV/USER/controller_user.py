# controller_category.py
# OM 2020.04.06 Gestions des "USER" FLASK pour les genres.
# copié et readapté par Luca Maggioli info 1b

import pymysql
from flask import render_template, flash, request, jsonify

from APP_RDV import obj_mon_application
from APP_RDV.USER.data_gestion_users import gestionUser

# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /add_genres ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# Pour la tester http://127.0.0.1:1234/add_genres
# ---------------------------------------------------------------------------------------------------
# @obj_mon_application.route("/", methods=['GET', 'POST'])
# def home_page():
#     return render_template("home.html")

@obj_mon_application.route("/home", methods=['GET', 'POST'])
def home():
    return render_template("home.html")


@obj_mon_application.route("/add_user", methods=['GET', 'POST'])
def add_user():

    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:

            obj_actions_genres = gestionUser()
            # OM 2019.03.17 Récupère le contenu du champ dans le formulaire HTML "add_category.html"
            name_user = request.form['name_user_html']
            surname_user = request.form['surname_user_html']
            # birth_date_str_MySqlFormat = request.form['bday_user_html']
            bday_day = request.form['bday_user_day_html']
            bday_month = request.form['bday_user_month_html']
            bday_year = request.form['bday_user_year_html']
            birth_date_str_MySqlFormat = bday_year+"-"+bday_month+"-"+bday_day

            #valeurs_insertion_dictionnaire = {"value_intitule_genre": name_user}

            obj_actions_genres.add_user(name_user, surname_user, birth_date_str_MySqlFormat)

            # # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash("Données insérées !!", "Sucess")
            print(' Données insérées !!')

        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as error:
            # OM 2019.03.25 Les 2 messages ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", error)
            flash("Gros problème dans l'ajout de user", "Danger")
    elif request.method == "GET":
        obj_actions_genres = gestionUser()
        data_roles = obj_actions_genres.get_roles()
        print(" data gentre s", data_roles, "type ", type(data_roles))

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("user/add_user.html", data=data_roles)

# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /afficher_genres ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# Pour la tester http://127.0.0.1:1234/afficher_genres
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/afficher_users", methods=['GET', 'POST'])
def afficher_users():

    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "GET":
        try:

            obj_actions_genres = gestionUser()

            data_users = obj_actions_genres.afficher_users()
            print(" data gentre s", data_users, "type ", type(data_users))

            # # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            message_html = 'Données Listées !!'
            print(' Données Listées !!')
        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as error:
            # OM 2019.03.25 Les 2 messages ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", error)
            flash("Gros problème dans l'affichage des genres  !", "Danger")


    # OM 2020.04.07 Envoie la page "HTML" au serveur. On passe des paramètres nécessaires à l'affichages des données
    # contenues dans la BD.
    return render_template("user/afficher_users.html", data=data_users)

@obj_mon_application.route("/home")
def afficher_home():

    return render_template("home.html")