from flask import Flask

obj_mon_application = Flask(__name__, template_folder="templates")

obj_mon_application.secret_key = '_TPD?nonPDT,ah_PDT!Patate._)?^'

from APP_RDV.USER import controller_user
from APP_RDV.ROLE import role_controller
from APP_RDV.DRAW import controller_draw
from APP_RDV.CATEGORY import controller_category
from APP_RDV.DRAW_CATEGORY import controller_draw_category