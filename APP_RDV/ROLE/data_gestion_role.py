# data_gestion_role.py
# OM 2698.03.21 Permet d'insérer plusieurs valeurs dans la table t_films
from datetime import datetime

from APP_RDV.DATABASE import connect_db_context_manager


class GestionRoles():
    def __init__(self):
        print("Classe constructeur GestionGenres ")

    def afficher_roles(self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            strsql_genres_afficher = "SELECT * FROM t_role"
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(strsql_genres_afficher)
                    data_roles = cursor.fetchall()
                    print("data_users : ", data_roles)
                    return data_roles
        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))

    def add_role(self, _role_Name): # _user_BirthDate
        try:
            print("Data from html: '" + _role_Name +"'")
            # date_time_bday = datetime.strptime(bday_str, '%Y-%m-%d')

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            #strsql_insert_genre = "INSERT INTO t_user (Name, Surname, BirthDate) VALUES ('"+_user_Name+"', '"+_user_Surname+"', "+bday_str+")"
            strsql_insert_genre = "INSERT INTO t_role (role_name) VALUES ('"+_role_Name+"')"
            with connect_db_context_manager.MaBaseDeDonne() as db:
                db.cursor().execute(strsql_insert_genre)


        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))
