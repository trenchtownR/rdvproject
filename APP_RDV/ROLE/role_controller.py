# role_controller.py
# Gestion des roles, ajouter ou supprimer des roles, fonction normalement à ne pas utiliser
# Luca Maggioli info 1b 06-05-2020

import pymysql
from flask import render_template, flash, request, jsonify

from APP_RDV import obj_mon_application
from APP_RDV.ROLE.data_gestion_role import GestionRoles

# ---------------------------------------------------------------------------------------------------
# Définition des "routes"
# ---------------------------------------------------------------------------------------------------

@obj_mon_application.route("/add_role", methods=['GET', 'POST'])
def add_role():

    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:

            obj_gestionRoles = GestionRoles()
            # Récupère le contenu du champ dans le formulaire HTML "add_role.html"
            name_role = request.form['name_role_html']

            #valeurs_insertion_dictionnaire = {"value_intitule_genre": name_user}

            obj_gestionRoles.add_role(name_role)

            # # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash("Données insérées !!", "Sucess")
            print(' Données insérées !!')
        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as error:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", error)
            flash("Gros problème dans l'ajout de user", "Danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("role/add_role.html")

# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /afficher_genres ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# Pour la tester http://127.0.0.1:1234/afficher_genres
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/afficher_roles", methods=['GET', 'POST'])
def afficher_roles():

    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "GET":
        try:

            obj_gestion_roles = GestionRoles()

            data_users = obj_gestion_roles.afficher_roles()
            print(" data gentre s", data_users, "type ", type(data_users))

            # # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            message_html = 'Données Listées !!'
            print(' Données Listées !!')
        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as error:
            # OM 2019.03.25 Les 2 messages ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", error)
            flash("Gros problème dans l'affichage des genres  !", "Danger")


    # OM 2020.04.07 Envoie la page "HTML" au serveur. On passe des paramètres nécessaires à l'affichages des données
    # contenues dans la BD.
    return render_template("role/afficher_roles.html", data=data_users)