# connect_db_context_manager.py
#
# OM 2020.04.05 Classe pour se connecter à la base de donnée.
#
# La notion en Python de "context manager" aide à simplifier le code.
# https://docs.python.org/3/library/stdtypes.html#typecontextmanager
# https://book.pythontips.com/en/latest/context_managers.html

# Le coeur du système pour la connxion à la BD
# Si on utilise MAMP il faut choisir import mysql.connector
# https://dev.mysql.com/downloads/connector/python/
import pymysql
#import pymysql.cursors
# Petits messages "flash", échange entre Python et Jinja dans une page en HTML
from flask import flash


class MaBaseDeDonne():
    # Quand on instancie la classe il interprète le code __init__
    def __init__(self):
        self.host = '127.0.0.1'
        self.port = "81"
        self.user = 'root'
        self.password = 'root'
        self.db = "maggioli_luca_rdv_info1b_2020"

        # self.con = None
        # self.cur = None
        # self.connexion_bd = None
        print("Avec CM BD  INIT !! ")

    # Après la méthode __init__ il passe à __enter__, c'est là qu'il faut surveiller le bon déroulement
    # des actions. en cas de problèmes il ne va pas dans la méthode __exit__
    def __enter__(self):
        try:
            # OM 2019.04.05 ON SE CONNECTE A LA BASE DE DONNEE
            # ATTENTION : LE MOT DE PASSE PEUT CHANGER SUIVANT LE SERVEUR MySql QUE VOUS CHOISSISSEZ !!! (Uwamp, Xampp, etc)
            # autocommit doit être à False, sa valeur est testée lors de la sortie de cette classe.
            self.connexion_bd = pymysql.connect(host=self.host,
                                                user=self.user,
                                                password=self.password,
                                                db=self.db,
                                                cursorclass=pymysql.cursors.DictCursor,
                                                autocommit=False)
            print("Avec CM BD  CONNECTEE, TOUT va BIEN !! ")
            print("self.con....", dir(self.connexion_bd), "type of self.con : ", type(self.connexion_bd))
            # Retourne toute la classe de connexion à la BD, ainsi on peut utiliser les méthodes comme "self.con.open"
            # return self.connexion_bd
            return self.connexion_bd
        # OM 2020.03.11 Il y a un problème avec la BD (non connectée, nom erronné, etc)
        #
        except (Exception) as error:
            # OM 2019.03.09 SI LA BD N'EST PAS CONNECTEE, ON ENVOIE AU TERMINAL DES MESSAGES POUR RASSURER L'UTILISATEUR.
            # Plusieurs façons d'envoyer des messages d'erreurs à l'utilisateur.
            print(f"Print console ... BD NON CONNECTEE, numéro de l'erreur : {error}")
            # Petits messages "flash", échange entre Python et Jinja dans une page en HTML
            flash(f"Flash....BD NON CONNECTEE, numéro de l'erreur : {error}")
            # raise, permet de "lever" une exception et de personnaliser la page d'erreur
            # voir fichier "run_mon_app.py"
            raise Exception('Raise exception... Impossible de connecter la BD ! {}'.format(str(error)))

    # Méthode de sortie de la classe, c'est là que se trouve tout ce qui doit être fermé
    # Si un problème (une exeption est levéee avant (__init__ ou __enter__) cette méthode
    # n'est pas interpretée
    def __exit__(self, exc_type, exc_val, traceback):
        # La valeur des paramètres sont "None" si tout s'est bien déroulé.
        print("exc_val ", exc_val)
        """
            Si la sortie se passe bien ==> commit. Si exception ==> rollback
            
            Tous les paramètres sont de valeur "None" s'il n'y a pas eu d'EXCEPTION.
            En Python "None" est défini par la valeur "False"
        """
        if exc_val is None:
            print("commit !! ")
            self.connexion_bd.commit()
        else:
            print("rollback !! ")
            self.connexion_bd.rollback()

        # Fermeture de la connexion à la base de donnée.
        self.connexion_bd.close()
        print("La BD est FERMEE !! ")

    #
    # def execute_mabd(self, sql, params=None):
    #     return self.execute(sql, params or ())
    #
    def tadadafetchall_(self):
        return self.connexion_bd.cursor().fetchall()
