-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2020 at 04:20 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maggioli_luca_rdv_info1b_2020`
--
DROP DATABASE IF EXISTS maggioli_luca_rdv_info1b_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS maggioli_luca_rdv_info1b_2020;

-- Utilisation de cette base de donnée

USE maggioli_luca_rdv_info1b_2020;


-- --------------------------------------------------------

--
-- Table structure for table `t_category`
--

CREATE TABLE `t_category` (
  `id_category` int(11) NOT NULL,
  `category_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_category`
--

INSERT INTO `t_category` (`id_category`, `category_name`) VALUES
(2, 'Old School'),
(3, 'Dotwork'),
(5, 'Tribal'),
(6, 'BlackWork'),
(7, 'Japanese'),
(8, 'Traditional'),
(9, 'Minimal');

-- --------------------------------------------------------

--
-- Table structure for table `t_draw`
--

CREATE TABLE `t_draw` (
  `id_draw` int(11) NOT NULL,
  `image` text,
  `draw_name` varchar(43) DEFAULT NULL,
  `draw_description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_draw`
--

INSERT INTO `t_draw` (`id_draw`, `image`, `draw_name`, `draw_description`) VALUES
(1, '/static/images/draws/il-est-vieux-original.png', 'Maccaud', 'ceci est une description d\'une image trés importante, elle parle d\'un homme de la 707 qui posséde quelque chose que nous ne possedont pas, et ce sont des années, de vie, la sienne.'),
(2, 'http://fr.web.img3.acsta.net/pictures/15/11/10/09/30/165611.jpg', 'draw afred ', 'franchement je suis entrai'),
(6, '/static/images/draws/paris.jpg', 'another Draw', 'this is pariiiissss'),
(9, '/static/images/draws/sciences-exercice-mp3.png', 'dessin', 'detest'),
(13, '/static/images/draws/mcd.png', 'mcd', 'pd');

-- --------------------------------------------------------

--
-- Table structure for table `t_draw_to_category`
--

CREATE TABLE `t_draw_to_category` (
  `id_draw_category` int(11) NOT NULL,
  `fk_draw` int(11) NOT NULL,
  `fk_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_draw_to_category`
--

INSERT INTO `t_draw_to_category` (`id_draw_category`, `fk_draw`, `fk_category`) VALUES
(3, 2, 5),
(4, 3, 3),
(6, 2, 2),
(8, 6, 3),
(12, 9, 9),
(13, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `t_role`
--

CREATE TABLE `t_role` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(34) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_role`
--

INSERT INTO `t_role` (`id_role`, `role_name`) VALUES
(1, 'Client'),
(2, 'Operateur');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `ID_User` int(11) NOT NULL,
  `Name` varchar(43) NOT NULL DEFAULT 'Nom',
  `Surname` varchar(54) NOT NULL DEFAULT 'Prenom',
  `BirthDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`ID_User`, `Name`, `Surname`, `BirthDate`) VALUES
(1, 'Maitre', 'Yi', '1998-05-17'),
(2, 'Maitre', 'Kun', '1970-01-12'),
(3, 'Maitre', 'Zyx', '1943-02-13'),
(4, 'Maitre', 'Shap Tuu', '1943-09-23'),
(5, 'Maitre', 'Xhi', '1971-02-12'),
(6, 'Lulu', 'Mega', '2009-09-12'),
(7, 'Lulu', 'Mega', '2009-09-12'),
(8, 'Lulu', 'Mega', '2009-09-12');

-- --------------------------------------------------------

--
-- Table structure for table `t_user_to_draw`
--

CREATE TABLE `t_user_to_draw` (
  `id_user_to_draw` int(11) NOT NULL,
  `fk_draw` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_user_to_role`
--

CREATE TABLE `t_user_to_role` (
  `id_user_to_role` int(11) NOT NULL,
  `fk_role` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_category`
--
ALTER TABLE `t_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `t_draw`
--
ALTER TABLE `t_draw`
  ADD PRIMARY KEY (`id_draw`);

--
-- Indexes for table `t_draw_to_category`
--
ALTER TABLE `t_draw_to_category`
  ADD PRIMARY KEY (`id_draw_category`);

--
-- Indexes for table `t_role`
--
ALTER TABLE `t_role`
  ADD PRIMARY KEY (`id_role`),
  ADD UNIQUE KEY `role_name` (`role_name`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`ID_User`);

--
-- Indexes for table `t_user_to_draw`
--
ALTER TABLE `t_user_to_draw`
  ADD PRIMARY KEY (`id_user_to_draw`);

--
-- Indexes for table `t_user_to_role`
--
ALTER TABLE `t_user_to_role`
  ADD PRIMARY KEY (`id_user_to_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_category`
--
ALTER TABLE `t_category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `t_draw`
--
ALTER TABLE `t_draw`
  MODIFY `id_draw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `t_draw_to_category`
--
ALTER TABLE `t_draw_to_category`
  MODIFY `id_draw_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `t_role`
--
ALTER TABLE `t_role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `ID_User` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_user_to_draw`
--
ALTER TABLE `t_user_to_draw`
  MODIFY `id_user_to_draw` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_user_to_role`
--
ALTER TABLE `t_user_to_role`
  MODIFY `id_user_to_role` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
