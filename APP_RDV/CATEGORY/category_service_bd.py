# OM 2698.03.21 Permet d'insérer plusieurs valeurs dans la table t_films
import pymysql
from flask import flash

from APP_RDV.DATABASE import connect_db_context_manager


class Category_Service():
    def __init__(self):
        print("Classe constructeur Draw_Service ")

    def select_category_to_delete (self, id_category_to_delete):
        try:
            print(id_category_to_delete)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_genre = "SELECT id_category, category_name FROM t_category WHERE id_category = '" + id_category_to_delete + "'"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(str_sql_select_id_genre)
                    data_category_to_delete = cursor.fetchall()
                    print("data_users : ", data_category_to_delete)
                    return data_category_to_delete

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_genre_data Gestions Genres numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_genre_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_genre_data d\'un genre Data Gestions Genres {erreur}")

    def update_category(self, id_of_category, category_name_to_update, values_updater):
        try:
            print(values_updater)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>
            str_sql_update_category_name = "UPDATE t_category SET category_name = '" + category_name_to_update + "' WHERE id_category = '"+id_of_category+"'"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit

            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(str_sql_update_category_name)
                    data_update_category = cursor.fetchall()
                    print("data_users : ", data_update_category)
                    return data_update_category

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_genre_data Data Gestions Genres numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_genre_data d\'un genre Data Gestions Genres {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"'Doublon !!! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_genre_data Data Gestions Genres numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_genre_data d'un genre DataGestionsGenres {erreur}")


    def afficher_category(self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            strsql_genres_afficher = "SELECT * FROM t_category"
            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(strsql_genres_afficher)
                    data_all_categoryes = cursor.fetchall()
                    print("data_users : ", data_all_categoryes)
                    return data_all_categoryes
        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))

    def add_category(self, category_name): # _user_BirthDate
        try:
            print("Data from html: '" + category_name + "'")

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            #strsql_insert_genre = "INSERT INTO t_user (Name, Surname, BirthDate) VALUES ('"+_user_Name+"', '"+_user_Surname+"', "+bday_str+")"
            strsql_insert_genre = "INSERT INTO t_category (category_name) VALUES ('" + category_name + "')"
            with connect_db_context_manager.MaBaseDeDonne() as db:
                db.cursor().execute(strsql_insert_genre)

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print("error message: {0}".format(erreur))

    def edit_category(self, id_category):
        try:
            print(id_category)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            # str_sql_id_genre = "SELECT id_category, name_category FROM t_category WHERE id_category = %(value_id_category)s"
            str_sql_category_to_edit = "SELECT id_category, category_name FROM t_category WHERE id_category = '" + id_category + "'"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit

            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(str_sql_category_to_edit)
                    data_category = cursor.fetchall()
                    print("data_users : ", data_category)
                    return data_category
            # with connect_db_context_manager().connexion_bd as mconn_bd:
            #     with mconn_bd as mc_cur:
            #         mc_cur.execute(str_sql_id_genre)
            #         data_one = mc_cur.fetchall()
            #         print("valeur_id_dictionnaire...", data_one)
            #         return data_one

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_genre_data Data Gestions Genres numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_genre_data d'un genre Data Gestions Genres {erreur}")

    def delete_genre_data(self, id_category_to_delete):
        try:
            str_sql_category_to_delete = "DELETE FROM t_category WHERE id_category = '"+id_category_to_delete+"'"

            with connect_db_context_manager.MaBaseDeDonne() as conn1:
                with conn1.cursor() as cursor:
                    cursor.execute(str_sql_category_to_delete)
                    data_category_deleted = cursor.fetchall()
                    print("data_users : ", data_category_deleted)
                    return data_category_deleted

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_genre_data Data Gestions Genres numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_genre_data d'un genre Data Gestions Genres {erreur}")
