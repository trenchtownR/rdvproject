# run_mon_app.py
#
# OM 2020.03.29 Démonstration de l'utilisation du microframework Flask
# Des USER différentes sont définies
# Le retour des données se fait grâce à une page en HTML et le langage JINJA
# Avec le traitement de certaines erreurs.

# Importation de la Class Flask
from flask import flash, render_template
from APP_RDV import obj_mon_application

@obj_mon_application.errorhandler(Exception)
def special_exception_handler(error):
    flash(error, "Danger")
    return render_template("home.html")

if __name__ == "__main__":
    # C'est bien le script principal "__main__" donc on l'interprète (démarre la démo d'utilisation de Flask).
    # Pour montrer qu'on peut paramétrer Flask :
    # On active le mode DEBUG
    # L'adresse IP du serveur mis en place par Flask pourrait être changée.
    # Pour ce fichier on change le numéro du port par défaut.
    print("obj_mon_application.url_map ____> ", obj_mon_application.url_map)
    obj_mon_application.run(debug=True,
                            host="127.0.0.1",
                            port="9999")